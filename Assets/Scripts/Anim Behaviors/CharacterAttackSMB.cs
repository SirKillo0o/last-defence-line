﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAttackSMB : SceneLinkedSMB<UnitController>
{
    public override void OnSLStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        m_MonoBehaviour.StopedAttacking();
    }
}