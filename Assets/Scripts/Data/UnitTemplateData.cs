﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Unit_Data", menuName = "Templates/Unit_Data", order = 1)]
public class UnitTemplateData : ScriptableObject
{
    public string Name;
    public int Level = 1;
    public float Health;
    public float Mana;
    public float AttackSpeed;

    public float Damage;

    [Header("For Character")]
    public float CurrentXP = 0;
    public float XpBase = 20;

    public float XpMultiplier = 1.2f;

    public float XP = 0;

    public int MaxLevel = 20;

    [Header("For AI")]
    public float AttackRange = 3f;
    public float SkillChancePerc = 0;
    public int AttackVariants = 1;
    
    public float XPtoGive = 10;

    public SkillTemplate[] skills;

    [Header("Both")]

    public float DamageMultiplier = 1.2f;
    
    public float BaseHealth = 20;
    public float HealthMultiplier = 1.1f;

    public virtual bool SpecialSkill(EnemyController controller){
        if(skills.Length == 0) {
            Debug.Log("This object hasn't implemented spells", this);
            return false;
        }

        SkillTemplate skill = skills[Random.Range(0, skills.Length-1)];
        skill.UseSkill(controller, this);

        return true;
    }
}
