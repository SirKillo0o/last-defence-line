﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SkillTemplate : ScriptableObject
{
    public abstract void UseSkill(EnemyController controller, UnitTemplateData enemyData);
}
