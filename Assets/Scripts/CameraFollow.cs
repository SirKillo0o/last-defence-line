﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	[SerializeField] private Transform _target;

	[SerializeField] private float _smoothSpeed = 0.125f;
	[SerializeField] private Vector3 _offset;
	[SerializeField] private Vector3 _rotationOffset;
	[SerializeField] private Space _offsetPositionSpace = Space.Self;
	[SerializeField] private bool _lookBehind = true;
	[SerializeField] private bool _lockYRotation = false;

	void LateUpdate () {
		Vector3 desiredPosition = _target.position
		+ (_target.forward * _offset.z) 
		+ (_target.up * _offset.y)
		+ (_target.right * _offset.x);
		Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, _smoothSpeed);
		transform.position = smoothedPosition;

		if(_lookBehind)
		{
			transform.rotation = _target.rotation;
		}
		else
		{
			if(_lockYRotation) transform.rotation = Quaternion.Euler(_rotationOffset.x, _target.rotation.y, _rotationOffset.z);
			else transform.rotation = Quaternion.Euler(_rotationOffset.x, _rotationOffset.y, _rotationOffset.z);
		}		
	}
}
