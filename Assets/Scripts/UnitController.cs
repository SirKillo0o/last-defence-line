﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    public UnitTemplateData data;

	public double AttackCycle {get; protected set;} = 0;

	public bool Defending {get; protected set;} = false;
	public bool Strafing {get; protected set;} = false;
	public double DamagedOnCycle {get; protected set;} = -1;
	public bool AttackAnimationRunning {get; protected set;} = false;
    public float MaxHealth {get; protected set;} = 0;
	public bool Dead {get; protected set;} = false;

	protected Animator _animator;
	protected Rigidbody _rigidbody;

    protected float distToGround;

	protected Collider collider;

	protected float RequiredXP;

	protected virtual void Start() {
		_animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
		collider = GetComponent<Collider>();
		SceneLinkedSMB<UnitController>.Initialise(_animator, this);
	}
	
	public void StopedAttacking(){
		AttackAnimationRunning = false;
	}

	protected void TakeDamage(float dmg){
		this.data.Health -= dmg;
		if(this.data.Health <= 0) {
			Die();
			Dead = true;

			if(collider) collider.enabled = false;
		}
	}

	protected bool IsGrounded(){
	return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
	}

	public void Die(){
		this._animator.SetFloat("Health", 0);
		this._animator.SetBool("Dead", true);
	}

	public float GetDamage() => data.Damage * Mathf.Pow(data.DamageMultiplier, data.Level - 1);
	public float GetMaxHealth() => data.BaseHealth * Mathf.Pow(data.HealthMultiplier, data.Level - 1);
	public void SetHealthForCurrentLevel()
	{
		this.MaxHealth = GetMaxHealth();
		this.data.Health = this.MaxHealth;
	}

	public float GetRequiredXP() => data.XpBase * Mathf.Pow(data.XpMultiplier, data.Level - 1);
	public void ReceiveXP(float XP)
	{
		this.data.XP += XP;
		if(this.data.XP >= this.RequiredXP)
		{
			this.data.XP = 0;
			this.data.Level++;
			SetHealthForCurrentLevel();
			UpdateHealthUI();
			this.RequiredXP = GetRequiredXP();
			NotificationController.LevelUPAlert.Invoke(this.data.Level);
			LevelUpCallback();
		}
		UpdateEXPUI();
	}

	public virtual void UpdateHealthUI()
	{
		Debug.Log("Implement ME! - UpdateHealthUI", this);
	}

	public virtual void UpdateEXPUI()
	{
		Debug.Log("Implement ME! - UpdateEXPUI", this);
	}

	public virtual void LevelUpCallback()
	{
		
	}
}
