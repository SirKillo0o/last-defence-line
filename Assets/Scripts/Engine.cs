﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Engine : MonoBehaviour
{
    [SerializeField] private GameObject SpawnPoint;
    [SerializeField] private Vector3 SpawnPointNoise;
    [SerializeField] private TextMeshProUGUI waveTimer;
    [SerializeField] private LevelTemplate[] levels; 
    [SerializeField] private float winAfter = 190f;
    public static GameObject castle {get; private set;} = null;
    public static GameObject player {get; private set;} = null;

    private bool spawning = false;
    private float time = 0;
    private int currentLevelIndex = 0;
    private float waveTime = 0;
    private bool finalWave = false;
    private float totalTime = 0;
    private void Start() {
        castle = GameObject.FindGameObjectWithTag("Castle");
        player = GameObject.FindGameObjectWithTag("Player");
        CursorController.Override = true;
        CursorController.UnlockCursor();
        NotificationController.GameStart.AddListener(() => 
        {
            StartSpawner();
            CursorController.Override = false;
        });

        NotificationController.GameOver.AddListener(() => {
            CursorController.Override = true;
            CursorController.UnlockCursor();
        });

        NotificationController.WinGame.AddListener(() =>{
            CursorController.Override = true;
            CursorController.UnlockCursor();
        });
    }

    private void FixedUpdate() {
        if(spawning){
            totalTime += Time.deltaTime;
        }

        if(!finalWave && spawning){
            time += Time.deltaTime;
            if(time > waveTime){
                SpawnCurrent();
                time = 0;
            }
            float timer = waveTime - time;

            float minutes = 0;
            if(timer > 60) minutes = Mathf.Floor(timer/60);

            float seconds = timer - minutes*60;
            waveTimer.text = "00:"+(minutes < 10 ? "0"+Mathf.Floor(minutes): Mathf.Floor(minutes).ToString())
            + ":"+ (seconds < 10 ? "0"+Mathf.Floor(seconds): Mathf.Floor(seconds).ToString());
        }
        else
        {
            waveTimer.text = "00:00:00";
        }

        if(totalTime > winAfter){
            NotificationController.WinGame.Invoke();
        }
    }

    public void StartSpawner(){
        if(!spawning){
            spawning = true;
            this.waveTime = levels[currentLevelIndex].waveDuration;
            SpawnCurrent();
        }
    }

    private void SpawnCurrent(){
        System.Random rnd = new System.Random();
        foreach (var enemyType in levels[currentLevelIndex].enemies)
        {
            for (int i = 0; i < enemyType.amount; i++)
            {
                float x  = SpawnPoint.transform.position.x + (UnityEngine.Random.Range(0, SpawnPointNoise.x) *  rnd.Next(0, 1) == 0? -1: 1);
                float y  = SpawnPoint.transform.position.y + (UnityEngine.Random.Range(0, SpawnPointNoise.y) *  rnd.Next(0, 1) == 0? -1: 1);
                float z  = SpawnPoint.transform.position.z + (UnityEngine.Random.Range(0, SpawnPointNoise.z) *  rnd.Next(0, 1) == 0? -1: 1);
                GameObject enemy = Instantiate(enemyType.prefab, new Vector3(x,y,z), Quaternion.identity);
                enemy.GetComponent<EnemyController>().Init(enemyType.data, enemyType.enemyLevel);
            }
        }
        currentLevelIndex++;
        if(currentLevelIndex > levels.Length-1) finalWave = true;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
