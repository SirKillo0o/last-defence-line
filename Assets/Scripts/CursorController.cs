﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour
{
    public static bool Override = false;
    private void LateUpdate() {
        if(Override) return;
        
        if (Input.GetKey(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public static void UnlockCursor() => Cursor.lockState = CursorLockMode.None;
    public static void LockCursor() => Cursor.lockState = CursorLockMode.Locked;
}
