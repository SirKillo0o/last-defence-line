﻿using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(BoxCollider))]
public class EnemyController : UnitController
{
    [SerializeField] private SliderManager healthBar = default;
    [SerializeField] private TextMeshProUGUI name = default;
    [SerializeField] private TextMeshProUGUI level = default;
    [SerializeField] private AudioSource deathSource = default;
	public const string ATTACK_TRIGGER = "Attack";

    private GameObject castle;
    private NavMeshAgent agent;

    private bool hasLanded = false;
    private bool targetAcquired = false;
    private Vector2 smoothDeltaPosition = Vector2.zero;
    private Vector2 velocity = Vector2.zero;
    private LookAt lookAt;

    private GameObject target;
    private bool playerDead;

    public void Init(UnitTemplateData dataTemplate, int lvl){
        castle = Engine.castle;
        this.data = Instantiate(dataTemplate);
        this.data.Level = lvl;
        this.level.text = "Level "+lvl;
        this.SetHealthForCurrentLevel();
        name.text = this.data.Name;
        this.MaxHealth = this.data.Health;
        healthBar.mainSlider.value = 100;
        healthBar.mainSlider.maxValue = 100;
        this.RequiredXP = this.GetRequiredXP();
    }

    protected override void Start() {
        agent = GetComponent<NavMeshAgent>();
        agent.updatePosition = false;
        lookAt = GetComponent<LookAt>();
        BoxCollider collider = GetComponent<BoxCollider>();
        this.distToGround = collider.bounds.extents.y;
        base.Start();
        CharacterController.PLAYER_DIED.AddListener(() => {this.playerDead = true;});
    }

    private void FixedUpdate() {
        if(!hasLanded && this.IsGrounded()){
            hasLanded = true;
            _rigidbody.constraints = _rigidbody.constraints | RigidbodyConstraints.FreezePositionY;
        }

        Think();
        
        Vector3 worldDeltaPosition = agent.nextPosition - transform.position;
        if (worldDeltaPosition.magnitude > agent.radius) transform.position = agent.nextPosition - 0.9f*worldDeltaPosition;
    }

    private void Attack(){
        if(!AttackAnimationRunning){
			this.AttackCycle++;
            agent.SetDestination(this.transform.position);
            transform.LookAt(target.transform);
            AttackAnimationRunning = true;
            int attackVariant = Random.Range(0, this.data.AttackVariants);
            _animator.SetInteger("NormalAttackType", attackVariant);
            _animator.SetTrigger("Attack");
        }
    }

    private void Think(){
        if(Dead) return;

        if(playerDead) StopAI();
        else{
            if(target != null && Vector3.Distance(target.transform.position, this.transform.position) <= this.data.AttackRange)
            {
                Attack();
            }
            else
            {
                if((Engine.castle.transform.position - this.transform.position).sqrMagnitude < 3*3)
                {
                    agent.SetDestination(castle.transform.position);
                    target = castle;
                }
                else if ((Engine.player.transform.position-this.transform.position).sqrMagnitude < 3*3) //3x3 Matrix
                {
                    agent.SetDestination(Engine.player.transform.position);
                    target = Engine.player;
                }
                else 
                {
                    agent.SetDestination(castle.transform.position);
                    target = castle;
                }
                
                transform.LookAt(target.transform.position);
                
                Move();
            }
        }
    }

    private void Move(){
        Vector3 worldDeltaPosition = agent.nextPosition - transform.position;

        // Map 'worldDeltaPosition' to local space
        float dx = Vector3.Dot (transform.right, worldDeltaPosition);
        float dy = Vector3.Dot (transform.forward, worldDeltaPosition);
        Vector2 deltaPosition = new Vector2 (dx, dy);

        // Low-pass filter the deltaMove
        float smooth = Mathf.Min(1.0f, Time.deltaTime/0.15f);
        smoothDeltaPosition = Vector2.Lerp (smoothDeltaPosition, deltaPosition, smooth);

        // Update velocity if time advances
        if (Time.deltaTime > 1e-5f)
            velocity = smoothDeltaPosition / Time.deltaTime;

        bool shouldMove = velocity.magnitude > 0.5f && agent.remainingDistance > agent.radius;

        // Update animation parameters
        _animator.SetBool("move", shouldMove);
        _animator.SetFloat ("velx", velocity.x);
        _animator.SetFloat ("vely", velocity.y);

        lookAt.lookAtTargetPosition = agent.steeringTarget + transform.forward;
    }

    private void OnTriggerExit(Collider other)
    {
        Weapon wp = other.GetComponent<Weapon>();
        if(!this.Dead && wp != null && wp.tag == "CharacterWeapon" && wp.IsAttacking() && !wp.SameAttackCycle(this.DamagedOnCycle)){
            this.DamagedOnCycle = wp.AttackCycle();
            this.TakeDamage(wp.GetDamage());
            healthBar.mainSlider.value = this.data.Health;
            healthBar.valueText.text = (this.data.Health / this.MaxHealth)*100 + "%";
            _animator.SetTrigger("GotHit");
            if(Dead) 
            {
                StopAI();
                wp.GiveXP(this.data.XPtoGive * Mathf.Pow(this.data.XpMultiplier, this.data.Level - 1));
                deathSource.Play();
            }
        }
    }

    void OnAnimatorMove ()
    {
        if(!Dead && _animator) transform.position = agent.nextPosition;
    }

    private void StopAI()
    {
        agent.isStopped = true;
        agent.SetDestination(this.transform.position);
    }
}
