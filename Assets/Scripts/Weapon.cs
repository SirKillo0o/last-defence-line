﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] public UnitController unitController;
    
    private BoxCollider collider = default;

    private void Start() {
        collider = GetComponent<BoxCollider>();
    }

    public float GetDamage() => unitController.GetDamage();
    public bool IsAttacking() => unitController.AttackAnimationRunning;
    public bool SameAttackCycle(double cycle) => unitController.AttackCycle == cycle;
    public double AttackCycle() => unitController.AttackCycle;

    public void GiveXP(float xp) => unitController.ReceiveXP(xp);
}
