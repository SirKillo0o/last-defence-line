﻿using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;

public class CastleController : MonoBehaviour
{
    [SerializeField] private ProgressBar _healthBar = default;
    [SerializeField] private float maxHealth = 1000;
    [SerializeField] private float health;

    private double _damagedOnCycle;

    private bool _dead = false;

    private bool _gameWon = false;

    private void Start() {
        health = maxHealth;
        SetupUI();
        NotificationController.WinGame.AddListener(() => _gameWon = true);
    }

    private void SetupUI()
	{
		_healthBar.usePerc = false;
		_healthBar.speed = 0;
		_healthBar.isOn = true;
		UpdateHealthUI();
	}

	public void UpdateHealthUI()
	{
		_healthBar.maxValue = maxHealth;
		_healthBar.currentPercent = health;
		_healthBar.textPercent.text = health.ToString();
	}

    
    private void OnTriggerExit(Collider other)
    {
        Weapon wp = other.GetComponent<Weapon>();
        if(!_dead && !_gameWon && wp != null && wp.tag == "EnemyWeapon" && wp.IsAttacking() && !wp.SameAttackCycle(this._damagedOnCycle)){
			this._damagedOnCycle = wp.AttackCycle();
            this.health -= wp.GetDamage();
            UpdateHealthUI();

            if(health <= 0)
            {
                _dead = true;
                NotificationController.GameOver.Invoke();
            }
        }
    }
}
