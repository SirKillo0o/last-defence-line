﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level_Data", menuName = "Templates/Level_Data", order = 1)]
public class LevelTemplate:ScriptableObject
{
    public EnemyCluster[] enemies;
    public float waveDuration;
}

[Serializable]
public class EnemyCluster{
    public UnitTemplateData data;
    public GameObject prefab;
    public int amount;

    public int enemyLevel = 1;
}
