﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Michsky.UI.ModernUIPack;

public class NotificationController : MonoBehaviour
{
    public static IntEvent LevelUPAlert = new IntEvent();
    public static UnityEvent GameOver = new UnityEvent();
    public static UnityEvent WinGame = new UnityEvent();
    
    public static UnityEvent GameStart = new UnityEvent();
    [SerializeField] private NotificationManager lvlUpPopup = default;
    [SerializeField] private ModalWindowManager startScreen = default;
    [SerializeField] private ModalWindowManager winScreen = default;
    [SerializeField] private ModalWindowManager loseScreen = default;
 
    private const string LVL_UP_TXT = "Level X";
    private void Start() {
        LevelUPAlert.AddListener((int lvl) =>{
            lvlUpPopup.description = LVL_UP_TXT.Replace("X", lvl.ToString());
            lvlUpPopup.UpdateUI();
            lvlUpPopup.OpenNotification();
        });


        WinGame.AddListener(() => winScreen.OpenWindow());
        GameOver.AddListener(() => loseScreen.OpenWindow());
        startScreen.OpenWindow();
    }

    public void StartGame() => GameStart.Invoke();
}

public class IntEvent:UnityEvent<int>{

}
