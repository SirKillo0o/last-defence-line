﻿
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.Events;

public enum Direction{
	Left = -1,
	Right = 1
}

[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (Rigidbody))]
public class CharacterController : UnitController {

	public static UnityEvent PLAYER_DIED = new UnityEvent();
	[SerializeField] private ProgressBar _healthBar = default;
	[SerializeField] private ProgressBar _expBar = default;
	[SerializeField] private float _acceleration = 0.1f;
    [SerializeField] private float _decelaration = 0.1f;

	[SerializeField] private float _minDistanceBetweenTransforms = 1f;
	[SerializeField] private float _maxSpeed = 5f;
	[SerializeField] private float _minSpeed = 1f;
	[SerializeField] private float _speedMultiplier = 10f;
	[SerializeField] private float _sidewaysMultipler = 3f;
	[SerializeField] private float _attackForce = 0.5f;
	[SerializeField] private float _invincibilityFrame = 1f;
    [SerializeField] private float mouseSensitivity = 100.0f;

	[SerializeField] private AudioSource dieSource = default;
	[SerializeField] private AudioSource hitSource = default;
	[SerializeField] private AudioSource levelUpSource = default;

    private float rotY = 0.0f;
    private float rotX = 0.0f;
	public const string ATTACK_TRIGGER = "Attack";
	public const string DEFEND_TRIGGER = "Defend";
	public const string STRAFE_TRIGGER = "Strafe";
	public float Speed {get{return _speed;}}
	private float _speed = 1;
	private float _forward = 0;
	private float _lastHitTime = 0;
	private bool _canGetHit = true;

	private bool controlsLocked = true;

	protected override void Start () {
		this.data = Instantiate(this.data);
		this.SetHealthForCurrentLevel();
		this.RequiredXP = this.GetRequiredXP();
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
		base.Start();
		SetupUI();
		
		_animator.SetFloat("Speed", _speed);

		NotificationController.GameStart.AddListener(() => controlsLocked = false);
		
		NotificationController.GameOver.AddListener(() => controlsLocked = true);

		
		NotificationController.WinGame.AddListener(() => controlsLocked = true);
	}

	private void FixedUpdate() {
		if(!controlsLocked)
		{
			Move();
			HandleMouse();
			if(!_canGetHit && _lastHitTime > _invincibilityFrame) _canGetHit = true;
			else _lastHitTime += Time.deltaTime;
		}
	}

	public void Attack () {
		if(!AttackAnimationRunning)
		{  
			this.AttackCycle++;
            AttackAnimationRunning = true;
            _animator.SetTrigger(ATTACK_TRIGGER);

			StopDefending();
			hitSource.Play();
		}

	}

	public void Defend () {
		if(!Defending)
		{
			AttackAnimationRunning = false;
			Defending = true;
			_speed = 0;
			_forward = 0;
			_animator.SetBool("Defending",Defending);
			_animator.SetTrigger(DEFEND_TRIGGER);
		}
	}

	public void Special(){
		if(!AttackAnimationRunning)
		{  
			this.AttackCycle++;
            AttackAnimationRunning = true;
            _animator.SetTrigger("Special");

			StopDefending();
		}
	}

	public void StopDefending(){
        if(Defending){
    		Defending = false;
		    _animator.SetBool("Defending",Defending);
        }
	}

    public void Move()
    {
		if(!AttackAnimationRunning && !Defending){
			float v = Input.GetAxis("Vertical");
			bool moving = v != 0;
			if(v > 0)
			{
			_forward +=  _acceleration * Time.deltaTime;
			_speed += _acceleration * Time.deltaTime;
			}	
			else if(v < 0 || _forward > 0 )
			{
				_forward -=  _decelaration * Time.deltaTime;
				_speed -= _decelaration * Time.deltaTime;
			}
			else if(_forward < 0) _forward = 0 ;

			float h = Input.GetAxis("Horizontal");

			if(_speed > _maxSpeed) _speed = _maxSpeed;
			if(_speed < _minSpeed) _speed = _minSpeed;
			if(moving && _forward > 1) _forward = 1;
			if(moving && _forward < -1) _forward = -1;
			// _rigidbody.velocity = new Vector3 (h * _speed, _rigidbody.velocity.y, v * _speed);
			AddRigidBodyForce(h, v);

			_animator.SetFloat("Direction", h);
			_animator.SetFloat("Forward", _forward);
			_animator.SetFloat("Speed", _speed);
		}else if(AttackAnimationRunning){
			_speed = _minSpeed;
			AddRigidBodyForce(0, _forward);
			_animator.SetFloat("Direction", 0);
			_animator.SetFloat("Forward", _forward);
			_animator.SetFloat("Speed", _speed);
		}
		else{
			_forward = 0;
			_speed = _minSpeed;
			AddRigidBodyForce(0, 0);
			_animator.SetFloat("Direction", 0);
			_animator.SetFloat("Forward", _forward);
			_animator.SetFloat("Speed", _speed);
		}
    }

	private void AddRigidBodyForce(float h, float v){
		var targetVelocity = new Vector3(h, 0, v);
		targetVelocity = transform.TransformDirection(targetVelocity);
		targetVelocity *= _speed * (h!= 0? _speedMultiplier: _speedMultiplier);

		var maxVelocityChange = 10.0f;
		// Apply a force that attempts to reach our target velocity
		var velocity = _rigidbody.velocity;
		var velocityChange = (targetVelocity - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = 0;
		_rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
	}

    private void HandleMouse(){        
        float mouseX = Input.GetAxis("Mouse X");
        rotY += mouseX * mouseSensitivity * Time.deltaTime;
        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;

        if(Input.GetMouseButton(0)) Attack();
        if(Input.GetMouseButton(1)) Special();
        //else StopDefending();
    }

	
    private void OnTriggerExit(Collider other)
    {
        Weapon wp = other.GetComponent<Weapon>();
        if(!this.Dead && _canGetHit && wp != null && wp.tag == "EnemyWeapon" && wp.IsAttacking() && !wp.SameAttackCycle(this.DamagedOnCycle)){
            _lastHitTime = 0;
			_canGetHit = false;
			this.DamagedOnCycle = wp.AttackCycle();
            this.TakeDamage(wp.GetDamage() / (this.Defending? 2: 1));
            _healthBar.currentPercent = this.data.Health;
            _healthBar.textPercent.text = this.data.Health.ToString();
			if(this.Dead) 
			{
				PLAYER_DIED.Invoke();
				NotificationController.GameOver.Invoke();
				dieSource.Play();
			}
        }
    }

	private void SetupUI()
	{
		_healthBar.usePerc = false;
		_healthBar.speed = 0;
		_healthBar.isOn = true;
		UpdateHealthUI();
		_expBar.maxValue = 100;
		_expBar.usePerc = true;
		_expBar.speed = 0;
		_expBar.isOn = true;
		UpdateEXPUI();
	}

	public override void UpdateHealthUI()
	{
		_healthBar.maxValue = data.Health;
		_healthBar.currentPercent = data.Health;
		_healthBar.textPercent.text = data.Health.ToString();
	}

	public override void UpdateEXPUI()
	{
		_expBar.currentPercent = (this.data.XP / this.GetRequiredXP()) * 100;
	}

	public void NewAttackCycle()
	{
		AttackCycle++;
	}

	public override void LevelUpCallback()
	{
		levelUpSource.Play();
	}

}